from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Endereco, Demanda



class DemandaInline(admin.TabularInline):
    model = Demanda 



class UserAdmin(admin.ModelAdmin):
    inlines = [
        DemandaInline,
    ]



class DemandaAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('anunciante', 'endereco', 'descricao', 'contato', 'status')
        }),
    )
    list_display = ('descricao', 'anunciante', 'status')
    search_fields = ['descricao']



admin.site.register(Endereco)
admin.site.register(Demanda, DemandaAdmin)
