from django.db import models

# https://docs.djangoproject.com/en/3.0/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
#from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User

#class User(AbstractUser):
#    telefone = models.CharField(max_length=50, blank=True)
#
#    REQUIRED_FIELDS = ['first_name', 'last_name', 'phone_number']

class Endereco(models.Model):
    rua = models.CharField(max_length=200)
    numero = models.IntegerField()
    complemento = models.CharField(max_length=200)
    bairro = models.CharField(max_length=200)
    cidade = models.CharField(max_length=200)
    estado = models.CharField(max_length=200)
    pais = models.CharField(max_length=200)
    cep = models.CharField(max_length=8)

    def __str__(self):
        return self.rua

class Demanda(models.Model):
    anunciante = models.ForeignKey(User, on_delete=models.PROTECT)
    endereco = models.ForeignKey(Endereco, on_delete=models.PROTECT)
    descricao = models.TextField(max_length=200)
    contato = models.CharField(max_length=50)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.descricao
