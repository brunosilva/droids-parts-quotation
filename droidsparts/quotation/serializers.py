from rest_framework import serializers
from django.contrib.auth.models import User
#from quotation.models import User, Endereco, Demanda
from quotation.models import Endereco, Demanda



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'



class EnderecoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Endereco
        fields = '__all__'



class DemandaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Demanda
        fields = '__all__'
