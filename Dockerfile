FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
# https://stackoverflow.com/a/41436850
#COPY entrypoint.sh /code/
#RUN chmod +x /code/entrypoint.sh
COPY . /code/
